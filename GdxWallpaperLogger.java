import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.net.HttpRequestBuilder;

public class GdxWallpaperLogger implements AppUsageLogger, Net.HttpResponseListener {

    String url = null;
    int interval = -1;
    String applicationName = null;

    @Override
    public void log() throws ParametersNotSetException {

        if(url == null || interval == -1 || applicationName == null) {
            throw new ParametersNotSetException();
        }

        else {
            final GdxWallpaperLogger gl = this;
            Runnable loggerRunnable = new Runnable() {
            public void run() {
                while (true)

                {
                    HttpRequestBuilder requestBuilder = new HttpRequestBuilder();
                    Net.HttpRequest request = requestBuilder.newRequest().method(Net.HttpMethods.GET).url(url + "?application=" + applicationName + "&interval=" + interval).build();
                    request.setTimeOut(1000 * 20);

                    System.out.println("sending request");
                    Gdx.net.sendHttpRequest(request, gl);

                    try {
                        Thread.sleep(1000 * interval);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            };
            new Thread(loggerRunnable).start();

        }
    }

    @Override
    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public void setInterval(int interval) {
        this.interval = interval;
    }

    @Override
    public void setApplicationName(String appname) {
        this.applicationName = appname;
    }

    @Override
    public void handleHttpResponse(Net.HttpResponse httpResponse) {
        System.out.println("got response");
    }

    @Override
    public void failed(Throwable t) {
        System.out.println("failed");
    }

    @Override
    public void cancelled() {
        System.out.println("cancelled");
    }
}
