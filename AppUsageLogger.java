public interface AppUsageLogger  {

    void log() throws ParametersNotSetException;
    void setUrl(String url);
    void setInterval(int interval); // seconds
    void setApplicationName(String appname);
}
